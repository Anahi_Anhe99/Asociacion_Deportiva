﻿using Asociacio.GUI.Administrador.Interfaces;
using Asociacion.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asociacio.GUI.Administrador.Controles
{
    /// <summary>
    /// Lógica de interacción para Usuario.xaml
    /// </summary>
    public partial class Usuario : UserControl, IRepositorioPanel
    {
        UsuarioManager manager;
        DataGrid dtg;
        public Usuario()
        {
            InitializeComponent();
            manager = FabricManager.Usuario();
            dtg = new DataGrid();
        }



        public string Error => manager.Error;

        public DataGrid AsignarDataGrid { set => dtg = value; }

        public void Cancelar()
        {

        }

        public void CargarDatosIniciales()
        {
        }


        public bool Eliminar()
        {
            Asociacion.COMMON.Entidades.Usuario usuario = dtg.SelectedItem as Asociacion.COMMON.Entidades.Usuario;
            return manager.Eliminar(usuario.Id);
        }

        public bool Guardar()
        {
            return manager.Crear(new Asociacion.COMMON.Entidades.Usuario()
            {
                Contrasenia = txbContrasenia.Text,
                Nombre = txbNombre.Text
            });
        }

        public void Reporte()
        {
            throw new NotImplementedException();
        }

        public bool Modificado()
        {
            Asociacion.COMMON.Entidades.Usuario usuario = dtg.SelectedItem as Asociacion.COMMON.Entidades.Usuario;
            usuario.Nombre = txbNombre.Text;
            usuario.Contrasenia = txbContrasenia.Text;
            return manager.Actualizar(usuario);
        }

        public void Editar()
        {
            Asociacion.COMMON.Entidades.Usuario usuario = dtg.SelectedItem as Asociacion.COMMON.Entidades.Usuario;
            txbNombre.Text = usuario.Nombre;
            txbContrasenia.Text = usuario.Contrasenia;
            txbId.Text = usuario.Id.ToString();
        }

        public void Buscar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void CargarTabla()
        {
            dtg.ItemsSource = manager.Leer;
        }

        public void LimpiarCampos()
        {
            txbNombre.Clear();
            txbContrasenia.Clear();
            txbId.Text = "";
        }
    }
}

