﻿using Asociacio.GUI.Administrador.Interfaces;
using Asociacion.BIZ;
using Asociacion.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asociacio.GUI.Administrador.Controles
{
    /// <summary>
    /// Lógica de interacción para ControlEquipos.xaml
    /// </summary>
    public partial class ControlEquipos : UserControl, IRepositorioPanel
    {
        EquipoManager manager;
        DeporteManager deporteManager;
        DataGrid dtg;
        public ControlEquipos()
        {
            InitializeComponent();
            manager = FabricManager.Equipo();
            dtg = new DataGrid();
            deporteManager = FabricManager.Deporte();
        }

        public string Error => manager.Error;

        public DataGrid AsignarDataGrid { set => dtg = value; }
        
        public void Cancelar()
        {
            txbNombre.Clear();
            cmbDeporte.Text = "";
        }

        public void CargarDatosIniciales()
        {
            cmbDeporte.ItemsSource = deporteManager.Leer;
        }

        public void CargarTabla()
        {
            dtg.ItemsSource= cmbDeporte.Text == ""? manager.Leer: manager.BuscarPorDeporte(cmbDeporte.Text);
        }

        public void Editar()
        {
            Equipo equipo = dtg.SelectedItem as Equipo;
            txbNombre.Text = equipo.Nombre;
            cmbDeporte.Text = Convert.ToString(equipo.Tipo);
        }

        public bool Eliminar()
        {
            Equipo equipo = dtg.SelectedItem as Equipo;
            return manager.Eliminar(equipo.Id);
        }

        public bool Guardar()
        {
            return manager.Crear(new Equipo()
            {
                Nombre = txbNombre.Text,
                Tipo = cmbDeporte.SelectedItem as Deporte
            })!=false;
        }

        public void LimpiarCampos()
        {
            txbNombre.Clear();
            cmbDeporte.Text = "";
        }

        public bool Modificado()
        {
            Equipo equipo = dtg.SelectedItem as Equipo;
            equipo.Nombre = txbNombre.Text;
            equipo.Tipo = cmbDeporte.SelectedItem as Deporte;
            return manager.Actualizar(equipo)!=false;
        }

        public void Reporte()
        {
            throw new NotImplementedException();
        }
    }
}
