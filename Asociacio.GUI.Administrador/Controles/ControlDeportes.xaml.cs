﻿using Asociacio.GUI.Administrador.Interfaces;
using Asociacion.BIZ;
using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asociacio.GUI.Administrador.Controles
{
    /// <summary>
    /// Lógica de interacción para Deporte.xaml
    /// </summary>
    public partial class ControlDeportes : UserControl,IRepositorioPanel
    {
        DeporteManager manager;
        DataGrid dtg;

        public ControlDeportes()
        {
            InitializeComponent();
            manager = FabricManager.Deporte();
            dtg = new DataGrid();
        }

        public string Error => manager.Error;

        public DataGrid AsignarDataGrid { set => dtg = value; }

        public List<Deporte> Buscar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Cancelar()
        {
            txbNombre.Clear();
        }

        public void CargarDatosIniciales()
        {
            
        }

        public void CargarTabla()
        {
            dtg.ItemsSource=manager.Leer;
        }

        public void Editar()
        {
            Deporte deporte = dtg.SelectedItem as Deporte;
            txbNombre.Text = deporte.Nombre;
        }

        public bool Eliminar()
        {
            Deporte deporte = dtg.SelectedItem as Deporte;
            return manager.Eliminar(deporte.Id);
        }

        public bool Guardar()
        {
            return manager.Crear(new Deporte()
            {
                Nombre = txbNombre.Text
            });
        }

        public void LimpiarCampos()
        {
            txbNombre.Clear();
        }

        public bool Modificado()
        {
            Deporte deporte = dtg.SelectedItem as Deporte;
            deporte.Nombre = txbNombre.Text;
            return manager.Actualizar(deporte);
        }

        public void Reporte()
        {
            throw new NotImplementedException();
        }
    }
}
