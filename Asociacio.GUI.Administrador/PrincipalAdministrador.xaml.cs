﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Asociacio.GUI.Administrador.Controles;
using Asociacio.GUI.Administrador.Interfaces;

namespace Asociacio.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para PrincipalAdministrador.xaml
    /// </summary>
    public partial class PrincipalAdministrador : Window
    {
        public PrincipalAdministrador()
        {
            InitializeComponent();
        }

        private void btnDeporte_Click(object sender, RoutedEventArgs e)
        {
            AbrirCatalogo("Deporte", new ControlDeportes());
        }

        private void btnEquipo_Click(object sender, RoutedEventArgs e)
        {
            AbrirCatalogo("Equipo", new ControlEquipos());
        }

        private void btnTorneo_Click(object sender, RoutedEventArgs e)
        {
            AbrirCatalogo("Torneo", new ControlTorneos());
        }

        private void btnUsuario_Click(object sender, RoutedEventArgs e)
        {
            AbrirCatalogo("Usuario", new Usuario());
        }

        private void AbrirCatalogo(string titulo, Control control)
        {
            CatalogoBase principal = new CatalogoBase(control);
            principal.skpContenedor.Children.Clear();
            principal.Title.Content = titulo;
            principal.skpContenedor.Children.Add(control);
            Contenedor.Children.Clear();
            Contenedor.Children.Add(principal);

        }
    }
}
