﻿using Asociacio.GUI.Administrador.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asociacio.GUI.Administrador
{
    public enum Accion
    {
        Nuevo,
        Editar
    }
    /// <summary>
    /// Lógica de interacción para CatalogoBase.xaml
    /// </summary>
    public partial class CatalogoBase : UserControl
    {

        Accion accion;
        IRepositorioPanel panel;

        public CatalogoBase(Control control)
        {
            InitializeComponent();
            HabilitarBotones(false);
            skpContenedor.IsEnabled = false;
            panel = control as IRepositorioPanel;
            panel.LimpiarCampos();
            panel.AsignarDataGrid = dtg;
            ActualizarTabla();
            panel.CargarDatosIniciales();
        }


        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsEnabled = v;
            btnEditar.IsEnabled = !v;
            btnEliminar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
        }

        private void ActualizarTabla()
        {
            dtg.ItemsSource = null;
            panel.CargarTabla();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarBotones(true);
            accion = Accion.Nuevo;
            skpContenedor.IsEnabled = true;
            panel.LimpiarCampos();
            ActualizarTabla();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (dtg.SelectedItem != null)
            {
                accion = Accion.Editar;
                panel.Editar();
                HabilitarBotones(true);
                skpContenedor.IsEnabled = true;
            }
        }


        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtg.SelectedItem != null)
                {
                    panel.Eliminar();
                    panel.CargarTabla();
                    MessageBox.Show("Elemento Eliminado con éxito", Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (accion == Accion.Nuevo)
                {

                    if (panel.Guardar())
                    {
                        ActualizarTabla();
                        HabilitarBotones(false);
                        panel.LimpiarCampos();
                        MessageBox.Show("Elemento creado con éxito", Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show(panel.Error, Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    if (panel.Modificado())
                    {
                        ActualizarTabla();
                        HabilitarBotones(false);
                        panel.LimpiarCampos();
                        MessageBox.Show("Elemento creado con éxito", Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show(panel.Error, Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Title.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarBotones(false);
            panel.LimpiarCampos();
            skpContenedor.IsEnabled = false;

        }

        private void dtg_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dtg.SelectedItem != null)
            {
                accion = Accion.Editar;
                panel.Editar();
                HabilitarBotones(true);
                skpContenedor.IsEnabled = true;
            }
        }
    }
}
