﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Asociacion.GUI.Admon.Formularios;

namespace Asociacion.GUI.Admon
{
    public partial class Admon : Form
    {
        public Admon()
        {
            InitializeComponent();
        }
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Admon());
        }
        #region mover ventana

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int Iparam);

        #endregion

        private void btnSlide_Click(object sender, EventArgs e)
        {
            MenuVertical.Width = MenuVertical.Width == 60 ? MenuVertical.Width = 225 : MenuVertical.Width = 60;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = !true;
            btnMaximizar.Visible = !false;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012,0);
        }
        private void AbrirForm(object FormHijo)
        {
            if (this.Contenedor.Controls.Count > 0)
            {
                this.Contenedor.Controls.RemoveAt(0);
            }
            Form hijo = FormHijo as Form;
            hijo.TopLevel = false;
            hijo.Dock = DockStyle.Fill;
            this.Contenedor.Controls.Add(hijo);
            this.Contenedor.Tag = hijo;
            hijo.Show();
        }

        private void btnEquipos_Click(object sender, EventArgs e)
        {
            AbrirForm(new Equipos());
        }

        private void btnDeportes_Click(object sender, EventArgs e)
        {
                AbrirForm(new Deportes());
        }
    }
}
