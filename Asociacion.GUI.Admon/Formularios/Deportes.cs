﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asociacion.GUI.Admon.Formularios
{
    public partial class Deportes : Form
    {
        public enum accion
        {
            Nuevo,
            Editar
        }
        accion accionDeporte;
        IDeportesManager manager;
        public Deportes()
        {
            InitializeComponent();
            HabilitarBotones(false);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
            accionDeporte = accion.Nuevo;
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool v)
        {
            btnCancelar.IsAccessible = v;
            btnEditar.IsAccessible = !v;
            btnEliminar.IsAccessible = !v;
            btnGuardar.IsAccessible = v;
            btnNuevo.IsAccessible = !v;
        }

        private void LimpiarCampos()
        {
            txbNombre.Clear();
            pictureBox2 = null;

        }

        private void ActualizarTabla()
        {
        //    dtg.SelectedItems = null;
        //    dtg = manager.Leer.OrderBy(p => p.Nombre).ToList();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
        //    LimpiarCampos();
        //    accionDeporte = accion.Editar;
        //    HabilitarBotones(true);
        //    Deporte _depo = this.dtg.su as Deporte;
        //    if (_depo != null)
        //    {
        //        txbNombre.Text = _depo.Nombre;
        //    }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }
    }
    }
