﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Asociacion.BIZ
{
    public class EquipoManager:GenericManager<Equipo>,IEquiposManager
    {
        IGenericRepository<Equipo> repositorio;
        public EquipoManager(IGenericRepository<Equipo> genericRepository) : base(genericRepository)
        {
            repositorio = genericRepository;
        }
        public List<Equipo> BuscarPorDeporte(string deporte)
        {
            return repositorio.Read.Where(p => p.Tipo.Nombre == deporte).ToList();
        }
    }
}
