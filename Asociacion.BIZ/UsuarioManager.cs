﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuarioManager
    {
        public UsuarioManager(IGenericRepository<Usuario> genericRepository) : base(genericRepository)
        {

        }
    }
}
