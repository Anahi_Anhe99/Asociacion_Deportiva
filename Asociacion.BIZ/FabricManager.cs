﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Validadores;
using Asociacion.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.BIZ
{
    public static class FabricManager
    {
        public static DeporteManager Deporte()
        {
            return new DeporteManager(new GenericRepository<Deporte>(new DeportesValidator()));
        }
        public static EquipoManager Equipo()
        {
            return new EquipoManager(new GenericRepository<Equipo>(new EquiposValidator()));
        }
        public static TorneoManager Torneo()
        {
            return new TorneoManager(new GenericRepository<Torneo>(new TorneosValidator()));
        }
        public static UsuarioManager Usuario()
        {
            return new UsuarioManager(new GenericRepository<Usuario>(new UsuariosValidator()));
        }
    }
}
