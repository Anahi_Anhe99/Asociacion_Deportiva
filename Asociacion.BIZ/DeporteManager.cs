﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.BIZ
{
    public class DeporteManager:GenericManager<Deporte>,IDeportesManager
    {
        public DeporteManager(IGenericRepository<Deporte> genericRepository) : base(genericRepository)
        {

        }
    }
}
