﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.BIZ
{
    public class TorneoManager:GenericManager<Torneo>,ITorneosManager
    {
        public TorneoManager(IGenericRepository<Torneo> genericRepository):base (genericRepository)
        {

        }
    }
}
