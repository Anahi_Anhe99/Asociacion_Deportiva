﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Asociacion.BIZ
{
    public abstract class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> Repository)
        {
            repository = Repository;
        }

        public string Error { get => repository.Error; set => throw new NotImplementedException(); }

        public bool Resultado => repository.Resultado;

        public bool Crear(T entidad)
        {
            return repository.Create(entidad);
        }

        public List<T> Leer => repository.Read;

        public bool Actualizar(T entidad)
        {
            return repository.Update(entidad);
        }
        
        public bool Eliminar(ObjectId id)
        {
            return repository.Delete(id);
        }

        public T BuscarPorId(ObjectId id)
        {
            return Leer.Where(e => e.Id == id).SingleOrDefault();
        }
    }
}
