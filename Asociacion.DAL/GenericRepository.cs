﻿using Asociacion.COMMON.Entidades;
using Asociacion.COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Asociacion.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        private AbstractValidator<T> Validator;
        private ValidationResult ResultadoDeValidacion;
        public string Error { get; set; }
        public bool Resultado { get; set; }

        public GenericRepository(AbstractValidator<T> validator)
        {
            Validator = validator;
            client = new MongoClient(new MongoUrl("mongodb://Anahi_Anhe:quesadilla99@ds014388.mlab.com:14388/anahiinventarios"));
            db = client.GetDatabase("anahiinventarios");
            Resultado = false;
            Error = "";
        }


        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public bool Create(T entidad)
        {
            try
            {
                entidad.Id = ObjectId.GenerateNewId();
                Collection().InsertOne(entidad);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<T> Read => Collection().AsQueryable().ToList();

        public bool Update(T entidadModificada)
        {
            try
            {
                return Collection().ReplaceOne(p => p.Id == entidadModificada.Id, entidadModificada).ModifiedCount == 1;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool Delete(ObjectId id)
        {
            try
            {
                return Collection().DeleteOne(p => p.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {

                return false;
            }



        }
    }
}
