﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Entidades
{
    public class Torneo:BaseDTO
    {
        public Deporte Deporte { get; set; }
        public int EquiposPermitidos { get; set; }
        public List<Equipo> Equipos { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaTermino { get; set; }
    }
}
