﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Entidades
{
    public class Partido
    {
        public Equipo Equipo_A { get; set; }
        public Equipo Equipo_B { get; set; }
        public int PuntosA { get; set; }
        public int PuntosB { get; set; }
    }
}
