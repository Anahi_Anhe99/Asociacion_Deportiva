﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Entidades
{
    public abstract class BaseDTO
    {
        public ObjectId Id { get; set; }
        public string Nombre { get; set; }
    }
}
