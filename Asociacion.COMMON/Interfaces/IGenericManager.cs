﻿using Asociacion.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        string Error { get; set; }
        bool Resultado { get; }
        bool Crear(T entidad);
        List<T> Leer { get; }
        bool Actualizar(T entidad);
        bool Eliminar(ObjectId id);
        T BuscarPorId(ObjectId id);
    }
}
