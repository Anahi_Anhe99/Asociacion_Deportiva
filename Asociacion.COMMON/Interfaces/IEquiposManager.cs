﻿using Asociacion.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Interfaces
{
    public interface IEquiposManager:IGenericManager<Equipo>
    {
    }
}
