﻿using Asociacion.COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Asociacion.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T: BaseDTO
    {
        string Error { get; set; }
        bool Resultado { get; set; }
        bool Create(T entidad);
        List<T> Read { get; }
        bool Update(T entidadModificada);
        bool Delete(ObjectId id);
    }
}
