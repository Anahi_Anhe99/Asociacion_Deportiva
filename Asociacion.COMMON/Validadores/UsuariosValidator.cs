﻿using Asociacion.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Validadores
{
    public class UsuariosValidator:GenericValidator<Usuario>
    {
        public UsuariosValidator()
        {
            RuleFor(p=>p.Contrasenia).NotNull().NotEmpty().Length(8,20).WithMessage("La contraseña debe incluír 8 caracteres como mínimo");
        }
    }
}
