﻿using Asociacion.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Validadores
{
    public class EquiposValidator:GenericValidator<Equipo>
    {
        public EquiposValidator()
        {
            RuleFor(p => p.Tipo).NotNull().NotEmpty();
        }
    }
}
