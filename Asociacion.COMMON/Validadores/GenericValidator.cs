﻿using Asociacion.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Validadores
{
    public abstract class GenericValidator<T>:AbstractValidator<T> where T:BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(p => p.Id).NotNull().NotEmpty();
            RuleFor(p => p.Nombre).NotNull().NotEmpty().Length(3,40);
        }
    }
}

