﻿using Asociacion.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Asociacion.COMMON.Validadores
{
    public class TorneosValidator:GenericValidator<Torneo>
    {
        public TorneosValidator()
        {
            RuleFor(p => p.Deporte).NotNull().NotEmpty();
            RuleFor(p => p.EquiposPermitidos).NotNull().NotEmpty();
            RuleFor(p => p.Equipos).NotNull().NotEmpty();
        }
    }
}
